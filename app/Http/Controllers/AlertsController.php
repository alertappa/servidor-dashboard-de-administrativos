<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AlertsController extends Controller
{
    public function index($state) {
    }

    public function views($state = 'send')
    {
        return view('alerts.index') ->with('state', $state);
    }

    public function single($id)
    {
        return view('alerts.single') ->with('id', $id);
    }

    public function create() {
        return view('alerts.create');
    }

    public function store(AlertRequest $request) {

    } 

    public function show($id) {

    }

    public function edit($id) {

    }

    public function update(Request $request, $id) {

    }

    public function destroy($id) {

    }
}
