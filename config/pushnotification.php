<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'AAAATrvFiM0:APA91bHNc9B_QxjH6B_fgf7yVWYgZ859HJVmYcGhNzXSxqwJAVY2XhISEMupnaa8zlUZ7p3sfFxr1IHFOlxhCFXnKTZtHxtOtossygdfBRl-MsZpvBmmliRvb5uQQKGDy6H7Bp6nz5VU',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAATrvFiM0:APA91bHNc9B_QxjH6B_fgf7yVWYgZ859HJVmYcGhNzXSxqwJAVY2XhISEMupnaa8zlUZ7p3sfFxr1IHFOlxhCFXnKTZtHxtOtossygdfBRl-MsZpvBmmliRvb5uQQKGDy6H7Bp6nz5VU',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
      'passPhrase' => '1234', //Optional
      'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true
  ]
];