@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- analytics (numbers) -->
			<div class="row">
		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-mobile"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Clientes</span>
		              <span class="info-box-number">1</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-graduation-cap"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Alumnos</span>
		              <span class="info-box-number">7</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->

		        <!-- fix for small devices only -->
		        <div class="clearfix visible-sm-block"></div>

		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Usuarios</span>
		              <span class="info-box-number">2</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-bullhorn"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Notificaciones</span>
		              <span class="info-box-number">0</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		    </div>
		    <div class="row">
		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-address-card-o"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Reportes</span>
		              <span class="info-box-number">0</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-comments-o"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Citatorios</span>
		              <span class="info-box-number">0</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->

		        <!-- fix for small devices only -->
		        <div class="clearfix visible-sm-block"></div>

		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-times-rectangle-o"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text"> Inasistencias</span>
		              <span class="info-box-number">0</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		        <div class="col-md-3 col-sm-6 col-xs-12">
		          <div class="info-box">
		            <span class="info-box-icon bg-aqua"><i class="fa fa-send-o"></i></span>

		            <div class="info-box-content">
		              <span class="info-box-text">Alertas enviadas</span>
		              <span class="info-box-number">20</span>
		            </div>
		            <!-- /.info-box-content -->
		          </div>
		          <!-- /.info-box -->
		        </div>
		        <!-- /.col -->
		    </div>
			<!-- end analytics -->

			<!-- analytics (graph)-->
			<div class="row">
				<div class="col-md-12">
					 <!-- interactive chart -->
					 <div class="box replace">
					  <div class="box-header with-border">
					    <i class="fa fa-bar-chart-o"></i>
					    <h3 class="box-title"> Estadisticas (en tiempo) entrada de estudiantes</h3>
					    <div class="box-tools pull-right">
					    	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				    	</div>
					  </div>
					  <div class="box-body">
					  	<div class="col-md-12">
					    	<div id="graph" style="width:100%;height:250px;"></div>
					    </div>
					  </div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- interactive chart -->
					<div class="box replace">
						<div class="box-header with-border">
							<i class="fa fa-bar-chart-o"></i>
							<h3 class="box-title"> Estadisticas (en cantidad) entrada de estudiantes</h3>
							<div class="box-tools pull-right">
						    	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					    	</div>
						</div>
						<div class="box-body">
							<div class="col-md-12">
								<div id="graph2" style="width:100%;height:250px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="box replace">
						<div class="box-header with-border">
							<i class="fa fa-bar-chart-o"></i>
							<h3 class="box-title"> Estadisticas (por mes) de envio de alertas</h3>
							<div class="box-tools pull-right">
						    	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					    	</div>
						</div>
						<div class="box-body">
							<div class="col-md-12">
								<div id="donut-mes"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="box replace">
						<div class="box-header with-border">
							<i class="fa fa-bar-chart-o"></i>
							<h3 class="box-title"> Estadisticas (por tipo de alertas) de envio de alertas</h3>
							<div class="box-tools pull-right">
						    	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					    	</div>
						</div>
						<div class="box-body">
							<div class="col-md-12">
								<div id="donut"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end assitance-->

		</div>
	</div>
</div>

<?php
	/*use Edujugon\PushNotification\PushNotification;

	$push  = new PushNotification('fcm');

	$push->setMessage([
			'notification' =>[
				'title' => 'Laravel',
				'body'	=>	'Hola mundo',
				'sound'	=>	'default'
			],
			'data' => [
				'extraPayLoad1' => 'value1',
				'extraPayLoad2' => 'value2'
			]
		])
	->setDevicesToken(['cIoiL82ivEM:APA91bEDU-7zX708BM3-mJhshvj8UG7pl51KnZhlsyNH0T58GHLxii5xhErRbA_QFCFGVimdoh7qVfAXMuBNr0Cuq5FLAaFruWW4krSs02v6Zp6EPp0_xZfU_OyjUVncnGKlpsu8Z3GK'])
	->send()
	->getFeedback();

	if($push->feedback->success > 0){
		echo "
		<script>
            new PNotify({
              title: 'Enviada',
              text: 'Alerta enviada correctamente',
              type: 'success',
              styling: 'bootstrap3'
          });
        </script>";

	}else{
		echo "
		<script>
            new PNotify({
              title: 'Error',
              text: 'No se ha enviado la alerta',
              type: 'error',
              styling: 'bootstrap3'
          });
        </script>";
	}
	*/
?>

<link rel="stylesheet" href="{{ asset('css') }}/morris.css">
<script src="{{ asset('js') }}/raphael-min.js"></script>
<script src="{{ asset('js') }}/morris.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {

		//$.noConflict(); // important!!

		Morris.Donut({
			element: 'donut-mes',
			data: [
				{label: "Enero", value: 98},
				{label: "Febrero", value: 33},
				{label: "Marzo", value: 56},
				{label: "Abril", value: 138},
				{label: "Junio", value: 73},
				{label: "Julio", value: 59}
			]
		});

		Morris.Donut({
			element: 'donut',
			data: [
				{label: "Reportes", value: 12},
				{label: "Citatorios", value: 30},
				{label: "Inasistencias", value: 20},
				{label: "Informativas", value: 50}
			]
		});

		var day_data = [	
		  {"period": "2012-09-30", "licensed": 3351, "sorned": 629},
		  {"period": "2012-09-29", "licensed": 3269, "sorned": 618},
		  {"period": "2012-09-20", "licensed": 3246, "sorned": 661},
		  {"period": "2012-09-19", "licensed": 3257, "sorned": 667},
		  {"period": "2012-09-18", "licensed": 3248, "sorned": 627},
		  {"period": "2012-09-17", "licensed": 3171, "sorned": 660},
		  {"period": "2012-09-16", "licensed": 3171, "sorned": 676},
		  {"period": "2012-09-15", "licensed": 3201, "sorned": 656}
		];

		Morris.Line({
			element: 'graph',
			data: day_data,
			xkey: 'period',
			ykeys: ['licensed', 'sorned'],
			labels: ['Licensed', 'SORN']
		});

		Morris.Line({
			element: 'graph2',
			data: day_data,
			xkey: 'period',
			ykeys: ['licensed', 'sorned'],
			labels: ['Licensed', 'SORN']
		});
	});
</script>
@endsection
