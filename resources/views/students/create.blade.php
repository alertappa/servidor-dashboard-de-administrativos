@extends('adminlte::layouts.app')

@section('htmlheader_title')	Registrar alumno 
@endsection


@section('main-content')

	@if(count($errors) > 0)
		@foreach($errors->all() as $error)
			<script>
	            new PNotify({
	              title: 'Error',
	              text: '{{ $error }}',
	              type: 'error',
	              styling: 'bootstrap3'
	          });
	        </script>
		@endforeach
	@endif

	<div class="box">
		<!--form-->
		{!! Form::open(['route' => 'students.store', 'method' => 'POST', 'class' => 'form-horizontal form-label-left']) !!}
			<div id="wizard_verticle" class="form_wizard wizard_verticle">
				<div class="box-body">
					<div class="col-md-12">
						<!--here-->
						<div class="form-group">
		        			{!! Form::label('last_name' , 'Apellido paterno', ['class' => 'control-label col-md-3 col-sm-3']) !!}
		        			<div class="col-md-6 col-sm-6 col-xs-12">
		        				{!! Form::text('last_name' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'MARTÍNEZ', 'required']) !!}
		        			</div>
		    			</div>
		    			<div class="form-group">
		        			{!! Form::label('last_name2' , 'Apellido materno', ['class' => 'control-label col-md-3 col-sm-3']) !!}
		        			<div class="col-md-6 col-sm-6 col-xs-12">
		        				{!! Form::text('last_name2' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'SÁNCHEZ', 'required']) !!}
		        			</div>
		    			</div>
		    			<div class="form-group">
		        			{!! Form::label('names' , 'Nombre(s) ', ['class' => 'control-label col-md-3 col-sm-3']) !!}
		        			<div class="col-md-6 col-sm-6 col-xs-12">
		        				{!! Form::text('names' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'JOSÉ', 'required']) !!}
		        			</div>
		    			</div>
		    			<div class="form-group">
		        			{!! Form::label('grade' , 'Grado actual', ['class' => 'control-label col-md-3 col-sm-3']) !!}
		        			<div class="col-md-6 col-sm-6 col-xs-12">
		        				{!! Form::select('grade' , ['1' => '1 (Primero)', '2' => '2 (Segundo)', '3' => '3 (Tercero)'], null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Seleccionar grado', 'required']) !!}
		        			</div>
		    			</div>
		    			<div class="form-group">
		        			{!! Form::label('group' , 'Grupo actual', ['class' => 'control-label col-md-3 col-sm-3']) !!}
		        			<div class="col-md-6 col-sm-6 col-xs-12">
		        				{!! Form::select('group' , ['A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F'], null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Seleccionar grupo', 'required']) !!}
		        			</div>
		    			</div>
		    			<div class="form-group">
							{!! Form::label('turn' , 'Turno actual', ['class' => 'control-label col-md-3 col-sm-3']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::select('turn' , ['Matutino' => 'Matutino', 'Vespertino' => 'Vespertino'], null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Seleccionar turno', 'required']) !!}
							</div>
						</div>
				        <div class="form-group">
							{!! Form::label('key' , 'Clave unica', ['class' => 'control-label col-md-3 col-sm-3']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::text('key' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'ASADE25', 'required']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('password' , 'Contraseña', ['class' => 'control-label col-md-3 col-sm-3']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::text('password' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'id' => 'pswView', 'readonly']) !!}
							</div>
						</div>
				        <div class="form-group">
							{!! Form::label('reports' , '# Reportes', ['class' => 'control-label col-md-3 col-sm-3']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::number('reports' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => '0', 'required']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('citations' , '# Citatorios', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::number('citations' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => '0', 'required']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('notifications' , '# Notificaciones', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::number('notifications' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => '0', 'required']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('non_attendance' , '# Inasistencias', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
							<div class="col-md-6 col-sm-6 col-xs-12">
								{!! Form::number('non_attendance' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => '0', 'required']) !!}
							</div>
						</div>
						<!--end-->
					</div>
					<!--<div class="col-md-2">
						<br><br><br>
						{!! QrCode:: size(250) -> color(100,200,250) -> generate('LAMJ98DSU'); !!}
					</div>-->
				</div>
			</div>
			<div class="box-footer">
				<div class="col-md-2 pull-right">
					{!! Form::submit('Registrar', ['class' => 'btn btn-primary btn-block']) !!}
				</div>
			</div>
	    {!! Form::close() !!}
		<!--end form-->
	</div>

@endsection

<script type="text/javascript" src="{{ asset('js/') }}/jquery.js"></script>
<script type="text/javascript">
	$( document ).ready(function() {
	    function rand_code(chars, lon){
			code = "";
			for (x=0; x < lon; x++){
				rand = Math.floor(Math.random()*chars.length);
				code += chars.substr(rand, 1);
			}
			return code;
		}
		
		function generarPSW() {
			caracteres = 'ABCDEFGHJKÑLMNPQRSTWXZ23456789';
			cajaLetra = document.getElementById('pswView');
			cajaLetra.value = rand_code(caracteres, 5)+ 'Ñ' + rand_code(caracteres, 6);
		}

		generarPSW();
	});
</script>