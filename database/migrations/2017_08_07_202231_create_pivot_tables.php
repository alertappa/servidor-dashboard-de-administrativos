<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        *   Tablas pivote
        */

        //student & client
        // Muchos clientes a muchos alumnos
        Schema::create('client_student', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('student_id')->unsigned();
            $table->string('is_master');
            $table->string('relationship');

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('student_id')->references('id')->on('students');
            
            $table->timestamps();
        });

        //alert & user
        // Muchos usuarios a una alerta
        Schema::create('alert_student', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alert_id')->unsigned();
            $table->integer('student_id')->unsigned();
            $table->string('state');

            $table->foreign('alert_id')->references('id')->on('alerts');
            $table->foreign('student_id')->references('id')->on('students');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_student');
        Schema::dropIfExists('alert_student');
    }
}
