<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get('email@example.com') }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{!! Request::is('home') ? 'active' : '' !!}"><a href="{{ url('home') }}"><i class='fa fa-home'></i> <span>Inicio</span></a></li>
            <li class="treeview {!! Request::is('students/*') ? 'active' : '' !!}">
                <a href="#"><i class='fa fa-graduation-cap'></i> <span> Alumnos</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{!! Request::is('students/create') ? 'active' : '' !!}"><a href="{{ route('students.create') }}"><i class="fa fa-circle-o"></i> Agregar</a></li>
                    <li class="{!! Request::is('students') ? 'active' : '' !!}"><a href="{{ route('students.index') }}"><i class="fa fa-circle-o"></i> Consultar</a></li>
                </ul>
            </li>
            <li class="{!! Request::is('clients') ? 'active' : '' !!}"><a href="{{ route('clients.index') }}"><i class='fa fa-mobile'></i> <span>Clientes</span></a></li>

            <li class="treeview {!! Request::is('alerts/*') ? 'active' : '' !!}">
              <a href="#">
                <i class="fa fa-bullhorn"></i> <span>Alertas</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('alerts.create') }}"><i class="fa fa-circle-o"></i> Enviar</a></li>
                <li><a href="{{ route('alerts', 'all') }}"><i class="fa fa-circle-o"></i> Todas</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Recibidas
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" >
                    <li><a href="{{ route('alerts', 'read') }}"><i class="fa fa-circle-o"></i> Leidas</a></li>
                    <li><a href="{{ route('alerts', 'unread') }}"><i class="fa fa-circle-o"></i> No Leidas</a></li>
                  </ul>
                </li>
                <li><a href="{{ route('alerts', 'pending') }}"><i class="fa fa-circle-o"></i> No recibidas</a></li>
              </ul>
            </li>


            <li class="treeview {!! Request::is('users/*') ? 'active' : '' !!}">
                <a href="#"><i class='fa fa-user'></i> <span> Usuarios</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('users.create') }}"><i class="fa fa-circle-o"></i> Agregar</a></li>
                    <li><a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i> Consultar</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
