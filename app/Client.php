<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = ['name', 'token_fcm', 'email', 'relationship', 'last_connection', 'phone_number'];

    public function students()
    {
        return $this->belongsToMany('App\Student');
    }
}
