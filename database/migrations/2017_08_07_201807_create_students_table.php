<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('grade');
            $table->string('group');
            $table->string('turn');
            $table->string('key')->unique(); // ID unico (numero de control, matricula, etc)
            $table->string('password');
            $table->integer('reports');
            $table->integer('citations');
            $table->integer('notifications');
            $table->integer('non_attendance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
