<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    protected $fillable = ['name', 'grade', 'group', 'turn', 'key', 'password', 'reports', 'citations', 'notifications', 'non_attendance'];

    public function alerts()
    {
        return $this->belongsToMany('App\Alert')->withTimestamps();
    }

    public function records()
    {
        return $this->belongsToMany('App\Record')->withTimestamps();
    }

    public function clients()
    {
        return $this->belongsToMany('App\Client')->withTimestamps();
    }
}