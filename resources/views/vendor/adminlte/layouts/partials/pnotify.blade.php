<!-- pnotify-->
    <script type="text/javascript" src="{{ asset('js/') }}/jquery.js"></script>
    <script type="text/javascript" src="{{ asset('js/') }}/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('js/') }}/pnotify.buttons.js"></script>
    @include('laravelPnotify::notify')
<!-- end PNotify -->