<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Jleon\LaravelPnotify\Notify;
use Yajra\Datatables\Datatables;

class ClientsController extends Controller
{
	public function index(){
    	$clients = Client::all();//orderBy('name', 'ASC');
    	//$students = Student::orderBy('name', 'ASC') -> pluck('name', 'id');
    	//$students = Student::orderBy('name', 'ASC') -> get();
        return view('clients.index') -> with('clients', $clients); //-> with('students', $students);
    }

    public function destroy($id)
    {
        $clients = Client::find($id);
        dd($clients);
    	//return dd($clients);
    }

    public function store(Request $request){

        $client = new Client($request -> all());
        $client -> save();

        Notify::info('Se ha registrado '.$client->name. ' de forma exitosa.', 'Listo');

        return redirect() -> route('students.index');
    }


    /*  API */

    /*
    *   Save client where Android App 
    */
    public function APIStorage(Request $request){ 
        $client = new Client($request -> all());
        $client -> save();

        return dd('Agregado exitosamente');
    }

    /*
    *   Ajax URL
    */
    public function getClients(){
        $clients = Client::select('*');
        return Datatables::of($clients)
                ->addColumn('actions', function ($client) {
                    return '<a href="'. route("clients.destroy", $client->id).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Ver</a>
                    <a href="'. route("clients.destroy", $client->id).'" class="btn btn-xs btn-danger" onclick="return confirm()"><i class="fa fa-trash-o"></i> Borrar</a>
                    '; })
                ->rawColumns(['actions'])
                ->make(true);
    }

}
