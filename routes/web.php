<?php

/* Route Defautl */
Route::get('/', function () {
    return redirect('/login');
});

/*  API for Android App (Professors and Clients) */
Route::group(['prefix' => 'api'], function() {
    Route::get('clients/create', [
        'uses'  =>  'ClientsController@APIStorage',
        'as'    =>  'api.clients.storage'
    ]);
    
    // Add route for state notifications
    // Add route for student<->client

    Route::get('records/storage', [
        'uses'  =>  'RecordsController@APIStorage',
        'as'    =>  'api.records.storage'
    ]);
});

/* Routes for web App */
Route::group(['middleware' => 'auth'], function () {
	Route::resource('students', 'StudentsController');
    Route::get('students/{id}/destroy', [
        'uses'  =>  'StudentsController@destroy',
        'as'    =>  'students.destroy'
    ]);
    Route::get('students/{id}/view', [
        'uses'  =>  'StudentsController@view',
        'as'    =>  'students.view'
    ]);
    Route::get('/getstudents', 'StudentsController@getStudents')->name('datatable.students'); // URL to Ajax

	Route::resource('clients', 'ClientsController' );
    Route::get('clients/{id}/destroy', [
        'uses'  =>  'ClientsController@destroy',
        'as'    =>  'clients.destroy'
    ]);
    Route::get('/getclients', 'ClientsController@getClients')->name('datatable.clients'); // URL to Ajax

    Route::get('alerts/create', [
        'uses'  =>  'AlertsController@create'
    ])->name('alerts.create');
	Route::get('alerts/show/{state}', [
		'uses'  =>  'AlertsController@views'
    ])->name('alerts');
    Route::get('alerts/view/{id}', [
        'uses'  =>  'AlertsController@single'
    ])->name('alerts.single');

	Route::resource('users', 'UsersController');
    Route::get('users/{id}/destroy', [
        'uses'  =>  'UsersController@destroy',
        'as'    =>  'users.destroy'
    ]);
    Route::get('/getusers', 'UsersController@getUsers')->name('datatable.users'); // URL to Ajax

    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
