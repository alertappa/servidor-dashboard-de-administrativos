@extends('adminlte::layouts.app')

@section('htmlheader_title')  Clientes registrados 
@endsection

@section('main-content')

<div class="row">
	<div class="col-md-3">
		<!-- Profile Image -->
		<div class="box box-primary">
			<div class="box-body box-profile">
				<div class="qr-code">
            		{!! QrCode::size(230)->generate($student -> key); !!}
            	</div>
            	<h3 class="profile-username text-center">{{ $student -> name }}</h3>
            	<p class="text-muted text-center">{{ $student -> key }}</p>
            	<ul class="list-group list-group-unbordered">
            		<li class="list-group-item">
            			<b>Reportes</b> <a class="pull-right">{{ $student -> reports }}</a>
        			</li>
    				<li class="list-group-item">
    					<b>Citatorios</b> <a class="pull-right"> {{ $student -> citations }}</a>
					</li>
					<li class="list-group-item">
						<b>Notificaciones</b> <a class="pull-right">{{ $student -> notifications }}</a>
					</li>
					<li class="list-group-item">
						<b>Innasistencias</b> <a class="pull-right">{{ $student -> non_attendance }}</a>
					</li>
				</ul>
				<a href="#" class="btn btn-primary btn-block"><b>Reportar</b></a>
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#Ajustes" data-toggle="tab">Informacion</a></li>
				<li><a href="#Reportes" data-toggle="tab">Reportes</a></li>
				<li><a href="#Citatorios" data-toggle="tab">Citatorios</a></li>
				<li><a href="#Notificaciones" data-toggle="tab">Notificaciones</a></li>
				<li><a href="#Innasistencias" data-toggle="tab">Inasistencias</a></li>
				<li><a href="#Informativas" data-toggle="tab">Informativas</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="Ajustes">
					<div class="row">
						<div class="col-md-12">
							<h3>{{ $student -> grade }}
							{{ $student -> group }}
							{{ $student -> turn }}
							{{ $student -> key }}
							{{ $student -> password }}</h3>
						</div>
					</div>
					<!--list devices-->
					<!--<div class="row"><div class="col-md-12"><h3>Dispositivos</h3></div></div>-->
					<div class="row">
						<!-- single device-->
						<div class="col-md-4">
					      <div class="box box-widget widget-user">
					        <div class="widget-user-header bg-aqua-active">
					          <h3 class="widget-user-username">NAME</h3>
					          <h5 class="widget-user-desc">EMAIL</h5>
					        </div>
					        <div class="widget-user-image">
					          <img class="img-circle" src="http://via.placeholder.com/128x128" alt="User Avatar">
					        </div>
					        <div class="box-footer">
					          <div class="row">
					            <div class="col-sm-6 border-right">
					              <div class="description-block">
					                <h5 class="description-header">NUMBER</h5>
					                <!--<span class="description-text">Telefono</span>-->
					              </div>
					              <!-- /.description-block -->
					            </div>
					            <!-- /.col -->
					            <div class="col-sm-6">
					              <div class="description-block">
					                <!--<h5 class="description-header">35</h5>-->
					                <span class="description-text">Telefono</span>
					              </div>
					              <!-- /.description-block -->
					            </div>
					            <!-- /.col -->
					          </div>
					          <div class="row">
					          	<div class="col-md-12"><button class="btn btn-primary btn-sm btn-block">ENVIAR</button></div>
					          </div>
					          <!-- /.row -->
					        </div>
					      </div>
					    </div>
					    <!--end single device-->
					</div>
					<!--end list devices-->
				</div>
				<div class="tab-pane" id="Reportes">
					<!--timeline-->
					<ul class="timeline timeline-inverse">
						<li class="time-label">
							<span class="bg-green">DATE</span>
						</li>
						<li>
							<i class="fa fa-paper-plane bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-exclamation"></i> ADMINISTRATOR</span>
								<h3 class="timeline-header"><a href="#">TITLE</a> </h3>
								<div class="timeline-body">
									CONTENT
								</div>
								<div class="timeline-footer">
									<label class="btn btn-warning btn-flat btn-xs">STATE</label>
								</div>
							</div>
						</li>
						<li><i class="fa fa-clock-o bg-gray"></i></li>
					</ul>
					<!--end timeline-->
				</div>
				<div class="tab-pane" id="Citatorios">
					<!--timeline-->
					<ul class="timeline timeline-inverse">
						<li class="time-label">
							<span class="bg-green">DATE</span>
						</li>
						<li>
							<i class="fa fa-paper-plane bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-exclamation"></i> ADMINISTRATOR</span>
								<h3 class="timeline-header"><a href="#">TITLE</a> </h3>
								<div class="timeline-body">
									CONTENT
								</div>
								<div class="timeline-footer">
									<label class="btn btn-warning btn-flat btn-xs">STATE</label>
								</div>
							</div>
						</li>
						<li><i class="fa fa-clock-o bg-gray"></i></li>
					</ul>
					<!--end timeline-->
				</div>
				<div class="tab-pane" id="Notificaciones">
					<!--timeline-->
					<ul class="timeline timeline-inverse">
						<li class="time-label">
							<span class="bg-green">DATE</span>
						</li>
						<li>
							<i class="fa fa-paper-plane bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-exclamation"></i> ADMINISTRATOR</span>
								<h3 class="timeline-header"><a href="#">TITLE</a> </h3>
								<div class="timeline-body">
									CONTENT
								</div>
								<div class="timeline-footer">
									<label class="btn btn-warning btn-flat btn-xs">STATE</label>
								</div>
							</div>
						</li>
						<li><i class="fa fa-clock-o bg-gray"></i></li>
					</ul>
					<!--end timeline-->
				</div>
				<div class="tab-pane" id="Innasistencias">
					<!--timeline-->
					<ul class="timeline timeline-inverse">
						<li class="time-label">
							<span class="bg-green">DATE</span>
						</li>
						<li>
							<i class="fa fa-paper-plane bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-exclamation"></i> ADMINISTRATOR</span>
								<h3 class="timeline-header"><a href="#">TITLE</a> </h3>
								<div class="timeline-body">
									CONTENT
								</div>
								<div class="timeline-footer">
									<label class="btn btn-warning btn-flat btn-xs">STATE</label>
								</div>
							</div>
						</li>
						<li><i class="fa fa-clock-o bg-gray"></i></li>
					</ul>
					<!--end timeline-->
				</div>
				<div class="tab-pane" id="Informativas">
					<!--timeline-->
					<ul class="timeline timeline-inverse">
						<li class="time-label">
							<span class="bg-green">DATE</span>
						</li>
						<li>
							<i class="fa fa-paper-plane bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-exclamation"></i> ADMINISTRATOR</span>
								<h3 class="timeline-header"><a href="#">TITLE</a> </h3>
								<div class="timeline-body">
									CONTENT
								</div>
								<div class="timeline-footer">
									<label class="btn btn-warning btn-flat btn-xs">STATE</label>
								</div>
							</div>
						</li>
						<li><i class="fa fa-clock-o bg-gray"></i></li>
					</ul>
					<!--end timeline-->
				</div>
            </div>
        </div>
    </div>
</div>


<div id="placeholder" style="width:600px;height:300px"></div>

<script type="text/javascript">
	$.plot($("#placeholder"), [ [[0, 0], [1, 1]] ], { yaxis: { max: 1 } });
</script>

@endsection