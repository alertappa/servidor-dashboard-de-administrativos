@extends('adminlte::layouts.app')

@section('htmlheader_title')  Clientes registrados 
@endsection

@section('main-content')

<div class="box">
    <div class="box-body">
        <table id="clients" class="table table-bordered table-striped" aria-describedby="Todos los clientes registrados en la plataforma">
            <thead>
                <tr>
                    <th style="width: 50%">Nombre</th>
                    <th style="width: 15%">Correo</th>
                    <th style="width: 15%">Activo</th>
                    <th style="width: 20%"># Acciones</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/') }}/jquery.js"></script>
<script type="text/javascript" src="{{ asset('js/') }}/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ asset('js/') }}/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
      $.noConflict(); // important!!
        oTable = $('#clients').DataTable({
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('datatable.clients') }}",
            "columns": [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'last_connection', name: 'last_connection'},
                {data: 'actions', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection