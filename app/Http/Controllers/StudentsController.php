<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use App\Student;
use Jleon\LaravelPnotify\Notify;
use Yajra\Datatables\Datatables;

class StudentsController extends Controller
{
    public function index(){
        return view('students.index');
    }

    public function create(){
    	return view('students.create');
    }

    public function store(StudentRequest $request){

        $student = new Student($request -> all());
        $student -> name = strtoupper($request -> names . ' ' . $request -> last_name . ' '. $request -> last_name2);
        $student -> save();

        Notify::info('Se ha registrado '.$student->name. ' de forma exitosa.', 'Listo');

        return redirect() -> route('students.index');
    }

    public function show($id){

    	/*
        
        $student = Student::find($id);

        $student -> username;
        $student -> name;
        $student -> rol;
        $student -> turn;
        

        dd($student);*/
    }

    public function edit($id)
    {
    	/*
        $student = Student::find($id);

        return view('admin.users.edit') -> with('student', $student);
        */
    }

    public function update(Request $request, $id)
    {
        //dd($request -> all());
    }

    public function destroy($id){
    	
        $student = Student::find($id);
        $student -> delete();

        Notify::info('El usuario '.$student->name. ' a sido borrado de manera exitosa.', 'Listo');
        return redirect() -> route('students.index');
    }

    public function view($id){
        
        $student = Student::find($id);

        return view('students.single')-> with('student', $student);
    }

    /*
    *   Ajax URL
    */
    public function getStudents(){
        $students = Student::select('*');
        return Datatables::of($students)
                ->addColumn('actions', function ($student) {
                    return '<a href="'. route("students.view", $student->id).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Ver</a>
                    <a href="'. route("students.destroy", $student->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Modificar</a>
                    <a href="'. route("students.destroy", $student->id).'" class="btn btn-xs btn-danger" onclick="return confirm()"><i class="fa fa-trash-o"></i> Borrar</a>
                    '; })
                ->rawColumns(['actions'])
                ->make(true);
    }
}
