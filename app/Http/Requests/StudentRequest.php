<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name'      =>  'min:3|max:30|required',
            'last_name2'      =>  'min:3|max:30|required',
            'names'      =>  'min:3|max:35|required',
            'grade'      =>  'required',
            'group'      =>  'required',
            'turn'      =>  'required',
            'key'      =>  'min:10|max:10|required',
            'password'  =>  'min:12|max:12|required',
        ];
    }
}
