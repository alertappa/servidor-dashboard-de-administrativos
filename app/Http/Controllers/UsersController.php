<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Jleon\LaravelPnotify\Notify;
use Yajra\Datatables\Datatables;

class UsersController extends Controller
{
    public function index(){
    	/*$users = User::all();//('name', 'ASC') -> paginate(5);
        return view('users.index') -> with('users', $users);
        */
        return view('users.index');
    }

    public function create(){
    	return view('users.create');
    }

    public function store(Request $request){

        $user = new User($request -> all());
        $user -> password = bcrypt($request -> password);
        //$last_connection = new DateTime();
        $user -> ip_address = $request->ip();
        $user -> last_connection = date('Y-m-d H:i:s');
        $user -> save();

        Notify::info('Se ha registrado '.$user->name. ' de forma exitosa.', 'Listo');

        return redirect() -> route('users.index');
    }

    public function show($id){

    	/*
        
        $student = Student::find($id);

        $student -> username;
        $student -> name;
        $student -> rol;
        $student -> turn;
        

        dd($student);*/
    }

    public function edit($id)
    {
    	/*
        $student = Student::find($id);

        return view('admin.users.edit') -> with('student', $student);
        */
    }

    public function update(Request $request, $id)
    {
        //dd($request -> all());
    }

    public function destroy($id){
    	
        $user = User::find($id);
        $user -> delete();

        Notify::info('El usuario '. $user->username . ' ('.$user->name. ') a sido borrado de manera exitosa.', 'Listo');
        return redirect() -> route('users.index');
    }


    // My functions
    public function getUsers(){
        $users = User::select('*');
        return Datatables::of($users)
                ->addColumn('actions', function ($user) {
                    return '<a href="'. route("users.destroy", $user->id).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Ver</a>
                    <a href="'. route("users.destroy", $user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Modificar</a>
                    <a href="'. route("users.destroy", $user->id).'" class="btn btn-xs btn-danger" onclick="return confirm()"><i class="fa fa-trash-o"></i> Borrar</a>
                    '; })
                ->rawColumns(['actions'])
                ->make(true);
    }
}
