@extends('adminlte::layouts.app')

@section('htmlheader_title') Agregar usuarios
@endsection


@section('main-content')

	@if(count($errors) > 0)
		@foreach($errors->all() as $error)
			<script>
            new PNotify({
              title: 'Error',
              text: '{{ $error }}',
              type: 'error',
              styling: 'bootstrap3'
          });
        </script>
		@endforeach
	@endif

	<div class="box box-success">
		{!! Form::open(['route' => 'users.store', 'method' => 'POST', 'class' => 'form-horizontal form-label-left']) !!}
			<div id="wizard_verticle" class="form_wizard wizard_verticle">
				<div class="box-body">
					<div class="form-group">
						{!! Form::label('username' , 'Nombre de usuario', ['class' => 'control-label col-md-3 col-sm-3']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::text('username' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Nombre usuario', 'required']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('name' , 'Nombre completo', ['class' => 'control-label col-md-3 col-sm-3']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::text('name' , null, ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Nombre completo', 'required']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('password' , 'Contraseña', ['class' => 'control-label col-md-3 col-sm-3']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::password('password' ,  ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => '************', 'required']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('role' , 'Rol', ['class' => 'control-label col-md-3 col-sm-3']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::select('role' , ['Administrador' => 'Administrador', 'Editor' => 'Editor', 'Colaborador' => 'Colaborador'], null,  ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Seleccione un rol', 'required']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('turn' , 'Turno', ['class' => 'control-label col-md-3 col-sm-3']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::select('turn' , ['Matutino' => 'Matutino', 'Vespertino' => 'Vespertino'], null,  ['class'=> 'form-control col-md-7 col-xs-12', 'placeholder' => 'Seleccione el turno', 'required']) !!}
						</div>
					</div>
					<div class="box-footer">
						{!! Form::submit('Registrar', ['class' => 'btn btn-success pull-right']) !!}
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
@endsection