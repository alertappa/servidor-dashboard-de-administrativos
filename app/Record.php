<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
	// Pending Modify
    protected $table = 'records';

    protected $fillable = ['student_id', 'type'];

    public function students()
    {
        return $this->belongsToMany('App\Student');
    }
}
